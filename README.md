# Date object

## Answers to theoretical questions

1. _Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування_  

Екранування - це спосіб обробки рядків, який дозволяє правильно відображати спец.символи в рядку, що за замовчуванням використовуються мовою програмування. Для екрануванян використовується символ зворотнього слешу.  

2. _Які засоби оголошення функцій ви знаєте?_  

**Function declaration**  
`function example() {code};`  

**Function expression**  
`const example = function() {code};`  

**Arrow function expression**  
`const example = () => code;`  

Окремо варто виділити методи об'єктів.  
  
3. _Що таке hoisting, як він працює для змінних та функцій?_  

Дослівно 'hoisting' означає 'підняття'. Під hoisting`ом у JavaScript розуміється можливість використання змінних та фунцій перед їх ініціалізацією. Такий підхід працюватиме зі змінною var та function declaration. Відмінністю буде лише те, що функції викликатимуться повноцінно, а от для змінних var значенням буде undefined. Використання змінних let та const перед їх оголошенням споводує помилку.