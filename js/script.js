function createNewUser() {
    let firstName, lastName, birthday;
    const newUser = {
        getAge() {
            const dateOfBirth = this.birthday.split('.');
            const reversedDate = dateOfBirth.reverse().join('-');

            const today = new Date();
            const birthday = new Date(reversedDate);
            
            let age = today.getFullYear() - birthday.getFullYear();
            
            const monthDifference = today.getMonth() - birthday.getMonth();
            const dayDifference = today.getDate() - birthday.getDate();
            
            if (monthDifference < 0 || (monthDifference === 0 && dayDifference < 0)) {
                age--;
            }
            
            return age;
        },
        getPassword() {
            
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        }
    };

    do {
        firstName = prompt('Enter your name (english letters only)');
        lastName = prompt('Enter your surname (english letters only)');
        birthday = prompt('Enter your bithday (dd.mm.yyyy)');
    } while (validateValue(firstName) || validateValue(lastName) || !validateDateFormat(birthday));

    Object.defineProperties(newUser, {
        'firstName': {
            get() {
                return this._firstName;
            },

            set(value) {
                this._firstName = value;
            }
        },
        'lastName': {
            get() {
                return this._lastName;
            },

            set(value) {
                this._lastName = value;
            }
        }
    });

    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.birthday = birthday;
    newUser.getLogin = function() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }

    return newUser;
}

function validateValue(str) {
    return !str || str.match(/[^a-z]/gi);
}

function validateDateFormat(date) {

    if (!date.match(/^\d{2}\.\d{2}\.\d{4}$/) || !date) {
      return false;
    }
    
    const dateElem = date.split('.');
    const day = parseInt(dateElem[0], 10),
          month = parseInt(dateElem[1], 10) - 1,
          year = parseInt(dateElem[2], 10);
          
    const dateExample = new Date(year, month, day);
    
    if (dateExample.getFullYear() !== year ||
        year <= new Date().getFullYear()-100 ||
        dateExample.getMonth() !== month ||
        dateExample.getDate() !== day ||
        dateExample > new Date()) {
      return false;
    }

    return true;
}

const user = createNewUser();

console.log(user);
console.log(user.getAge());
console.log(user.getPassword());